var urlFromForm = 'Please enter a valid image URL'; // $("#originalUrl");

// document.ready function

(function($) {

    $("#generatedLink").hide();
    //Scrolling - Anchord links
    $('*[data-scrollto]').click(function(event) {
        event.preventDefault();
        var scrollto = $(this).data('scrollto');
        var secTop = parseInt($('#' + scrollto).offset().top) + 100;
        $("html, body").animate({ scrollTop: secTop }, 1000);
        return false;
    });


    function onMouseHover(elem) {
        console.log(elem);
    }

    //Equal Height Columns
    if ($('.row.equal-height').length > 0) {
        $('.row.equal-height').each(function(idx, row) {
            var highestBox = 0;
            $('div[class^="col-"]', this).each(function() {
                // If this box is higher than the cached highest then store it
                if ($(this).height() > highestBox) {
                    highestBox = $(this).height();
                }
            });
            $('div[class^="col-"]', this).css('min-height', highestBox);
        });
    }

    //DROPZONE
    var dropzoneSettings = {
        url: 'index.html',
        method: 'post',
        parallelUploads: 1,
        uploadMultiple: false,
        maxFilesize: 2048,
        paramName: 'userImage',
        maxFiles: null,
        clickable: '#imageDropzone, #imageDropzone small',
        acceptedFiles: 'image/*',
        autoProcessQueue: true,
        init: function() {
            this.on("complete", function(file) {
                resizeImageAndProcessRequest(file.dataURL);
                this.removeAllFiles(true);
            });
        }
    };
    $("div#imageDropzone").dropzone(dropzoneSettings);

    //URL
    // selector has to be . for a class name and # for an ID
    $('#urlForm').submit(function(e) {
        e.preventDefault(); // prevent form from reloading page

        var urlFromForm = $("#originalUrl").val();

        var instaRegex = /(https?:\/\/www\.)?instagram\.com(\/p\/\w+\/?)/;
        var imageRegex = /(http|ftp|https):\/\/([\w+?\.\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)_\-\\=\\+\\\\\/\\?\\.\\:\\;\\'\\,]*\.(?:jpg|JPG|jpeg|JPEG|gif|GIF|png|PNG|bmp|BMP|tiff|TIFF))?/;
        if (!instaRegex.test(urlFromForm) && !imageRegex.test(urlFromForm)) {
            alert('Please enter a valid image URL');
            return false;
        }

        $("#loading").html('Loading response...');
        // getLocation(function(result) {
        processApiRequest($, 'url', urlFromForm);
        // });

        return false;
    });

    //CAPTURE
    $('#imageCaptureField').on('change', function(e) {
        var file = e.target.files[0];
        var reader = new FileReader();
        reader.onload = function(e) {
            resizeImageAndProcessRequest(e.target.result);
        };
        reader.readAsDataURL(file);
    });

    /**
     * Process API request
     * @param  {jquery} $       jQuery Object
     * @param  {String} type    Type of request, either "image" or "url".
     * @param  {String} dataURL Data url of image.
     * @return {null}
     */
    function processApiRequest($, type, dataURL) {

        var ajaxData, ajaxUrl;
        if (type == 'url') {
            ajaxUrl = "https://endpoint.p.rapidapi.com/url";
            ajaxData = "{\n\"imageUrl\": \"" + dataURL + "\"\n}";

        } else if (type == 'image') {
            ajaxUrl = "https://endpoint.p.rapidapi.com/upload";
            console.log(dataURL);
            var imageData = dataURL.substring(parseInt(dataURL.indexOf(',')) + 1);
            ajaxData = "{\n\"base64Image\": \"" + imageData + "\"\n}";

        }

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": ajaxUrl,
            "method": "POST",
            "headers": {
                "Content-Type": "application/json",
                "X-RapidAPI-Key": "b41d56aa18msha880a225a4c6c55p1664d7jsn1b9ddb547490"
            },
            "processData": false,
            "data": ajaxData
        };

        $.ajax(settings).done(function(response) {
            if (response.length > 60) {
                // destinationInfo = response;
                $("#generatedLink").show();
                $("#loading").hide();
                $(".landmark").text('');
                // $(".location_img").attr('src','');
                // $("#airports").html('');
                $("#landmark").html('');
                // $("#generatedLink").show();

                $(".landmark").text(response);

                if (dataURL.indexOf('www.instagram.com') > -1) {
                    $.get('https://api.instagram.com/oembed/?callback=&url=' + dataURL, function(data) {
                        $(".location_img").attr('src', data.thumbnail_url);
                    })
                } else {
                    $(".location_img").attr('src', dataURL);
                }
            } else {
                $("#generatedLink").hide();
                $("#loading").html('Not detected.');
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            var responseHtml = 'Not detected.<br>' + errorThrown;
            $("#generatedLink").html(responseHtml);
        }).always(function() {

        });
    }

    /**
     * Resize image before sending to api.
     * @param  {String} dataURL image data url (base64 encoded image)
     * @return {null}
     */
    function resizeImageAndProcessRequest(dataURL) {
        var nimg = $("<img/>").attr({ "src": dataURL, 'id': 'capturedImage', 'style': 'display: none;' });

        $.when(nimg.prependTo('body')).then(function() {
            setTimeout(function() {

                //Resize Image
                var originalWidth = nimg.outerWidth(),
                    originalHeight = nimg.outerHeight(),
                    aspectRatio = (originalWidth / originalHeight),
                    newWidth = 300,
                    newHeight = parseInt(newWidth / aspectRatio);

                var canvas = document.createElement("canvas");
                canvas.width = newWidth;
                canvas.height = newHeight;
                var context = canvas.getContext("2d");
                var imageObj = document.getElementById('capturedImage');
                context.drawImage(imageObj, 0, 0, newWidth, newHeight);
                nimg.remove();

                $("#loading").html('Analysing image ...');
                //Send request to api
                // processApiRequest($, 'image', canvas.toDataURL());
                processApiRequest($, 'image', dataURL);

                // });

            }, 2000);

        }, null);
    }

})(jQuery);